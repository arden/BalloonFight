﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace Assets.coreScripts.fighting.view
{
    public class FightingBaseView:View
    {
        [Inject]
        public IEventDispatcher dispatcher { get; set; }

        public virtual void GameUpdate() { }

        public virtual void Init() { }
    }
}
