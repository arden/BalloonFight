﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.common;
using Assets.coreScripts.main.model;

namespace Assets.coreScripts.fighting.view
{
    public class ReelBackgroundMediator : FightingBaseEventMediator
    {
        [Inject]
        public ReelView view { get; set; }

        [Inject]
        public User user { get; set; }

        [Inject]
        public DataBaseCommon database { get; set; }

        public override void OnRegister()
        {
            //view.Init(database.GetStageConfigByID(user.CurrentStateID));
            UpdateListeners(true);
        }
        public override void OnRemove()
        {
            UpdateListeners(false);
        }
        protected override void UpdateListeners(bool enable)
        {
            dispatcher.UpdateListener(enable, GameConfig.CoreEvent.USER_TOUCH, StartReelBackground);
            dispatcher.UpdateListener(enable, GameConfig.CoreEvent.GAME_UPDATE, view.GameUpdate);
            base.UpdateListeners(enable);
        }

        private void StartReelBackground()
        {
            view.Init(database.GetConfigByID(user.CurrentStateID,database.StageConfigList));
        }

        protected override void OnGameOver()
        {
            view.Stop();
            base.OnGameOver();
        }
    }
}
