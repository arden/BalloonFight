﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.model;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting.view
{
    [Obsolete("Please Use ObstructionView",true)]
    public class BallonDetector:EventView
    {

        public GameConfig.BallonType CurrentBallonType = GameConfig.BallonType.ENMY;


        public void OnTriggerEnter2D(Collider2D other)
        {
            CustomHitEventData data = new CustomHitEventData();
            data.targetObj = gameObject;

            if (other.gameObject.tag.ToUpper() == GameConfig.BallonType.ENMY.ToString() && CurrentBallonType == GameConfig.BallonType.PLAYER)
            {
                data.hitModel = GameConfig.ObstructionModel.PLAYER_BALLON_HURT;
                data.type = GameConfig.ObstructionModel.PLAYER_BALLON_HURT;
                dispatcher.Dispatch(GameConfig.ObstructionModel.PLAYER_BALLON_HURT, data);
            }

            if (other.gameObject.tag.ToUpper() == GameConfig.BallonType.PLAYER.ToString() && CurrentBallonType == GameConfig.BallonType.ENMY)
            {
                data.hitModel = GameConfig.ObstructionModel.ENEMY_BALLON_HURT;
                data.type = GameConfig.ObstructionModel.ENEMY_BALLON_HURT;
                dispatcher.Dispatch(GameConfig.ObstructionModel.ENEMY_BALLON_HURT, data);
            }
        }
    }
}
