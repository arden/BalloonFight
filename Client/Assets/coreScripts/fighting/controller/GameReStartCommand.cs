﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.util;
using strange.extensions.command.impl;

namespace Assets.coreScripts.fighting.controller
{
    public class GameReStartCommand : EventCommand
    {
        [Inject]
        public IGameTimer timer { get; set; }
        public override void Execute()
        {
            timer.Start();
        }
    }
}
